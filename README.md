# nathanward/image-compression

## Installation Steps
1. `composer require nathanward/image-compression`
2. `composer require tinify/tinify`
3. Publish config file `php artisan vendor:publish`
4. Add `TINIFY_KEY` to `.env` file

## Example Usage
```php
    use NathanWard\ImageCompression\ImageCompression;

    public function store(Request $request)
    {
        $file = $request->file;

        $path = (new ImageCompression($file))
            ->compress()
            ->store($file->getClientOriginalName(), 'files');

        return redirect()
            ->back()
            ->with('success', 'Image compressed and stored.');
    }
```

## TODO
1. Write basic tests.
2. Allow for multiple images to be compressed.
3. Create additional drivers.
