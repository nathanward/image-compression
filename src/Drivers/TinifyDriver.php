<?php

namespace NathanWard\ImageCompression\Drivers;

use Tinify\Source;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class TinifyDriver extends Driver
{
    protected $file;

    /**
     * Compress the given image.
     *
     * @param  \Illuminate\Http\UploadedFile $file
     * @return $this
     */
    public function compress(UploadedFile $file)
    {
        $this->file = \Tinify\Source::fromFile($file->getRealPath())->result();

        return $this;
    }

    /**
     * Store the given image.
     *
     * @param  string $name
     * @param  string $path
     * @return int
     */
    public function store(string $name, string $path, string $disk = 'local')
    {
        return Storage::disk($disk)->put("{$path}/{$name}", $this->file->data(), 'public');
    }
}
