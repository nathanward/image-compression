<?php

namespace NathanWard\ImageCompression\Drivers;

use Illuminate\Http\UploadedFile;

abstract class Driver
{
    /**
     * Compress given image.
     *
     * @param  \Illuminate\Http\UploadedFile $file
     * @return $this
     */
    abstract public function compress(UploadedFile $file);

    /**
     * Store the given image.
     *
     * @param  string $name
     * @param  string $path
     * @param  string $disk
     * @return boolean
     */
    abstract public function store(string $name, string $path, string $disk);
}
