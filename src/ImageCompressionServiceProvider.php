<?php

namespace NathanWard\ImageCompression;

use Illuminate\Support\ServiceProvider;

class ImageCompressionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/image-compression.php' => config_path('image-compression.php'),
        ]);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DriverManager::class, function ($app) {
            return new DriverManager($app);
        });
    }
}
