<?php

namespace NathanWard\ImageCompression;

use NathanWard\ImageCompression\Drivers\TinifyDriver;
use NathanWard\ImageCompression\Drivers\ShortPixelDriver;
use Illuminate\Support\Manager;
use Tinify\Tinify;
use ShortPixel\ShortPixel;

class DriverManager extends Manager
{
    /**
     * Create a Tinify driver instance.
     *
     * @return \NathanWard\ImageCompression\Drivers\TinifyDriver
     */
    public function createTinifyDriver()
    {
        $tinify = new Tinify;
        $tinify->setKey(config('image-compression.tinify.key'));

        return new TinifyDriver($tinify);
    }

    /**
     * Get the default session driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        if (is_null($this->app['config']['image-compression.driver'])) {
            return 'null';
        }

        return $this->app['config']['image-compression.driver'];
    }
}
