<?php

namespace NathanWard\ImageCompression;

use NathanWard\ImageCompression\DriverManager;
use Illuminate\Http\UploadedFile;

class ImageCompression
{
    /**
     * The file to be compressed.
     *
     * @var \Illuminate\Http\UploadedFile
     */
    protected $file;

    /**
     * Create a new ImageCompression instance.
     *
     * @param  \Illuminate\Http\UploadedFile $file
     * @return void
     */
    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Compress the file.
     *
     * @return mixed
     */
    public function compress()
    {
        return app(DriverManager::class)->compress($this->file);
    }
}
