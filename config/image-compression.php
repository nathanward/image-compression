<?php

return [
    'driver' => env('IMAGE_COMPRESSION_DRIVER', 'tinify'),
    'tinify' => [
        'key' => env('TINIFY_KEY', '')
    ]
];
